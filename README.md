# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

### Installation

```
npm install
```

### Local Development

```
npm start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
npm run build
```

This command generates static content into the `public` directory and can be served using any static contents hosting service.

### How to Use

See the [Welcome page](docs/welcome.md) for more information.

Also read the companion [JedFonner.com blog article](https://jedfonner.com/2023/01/22/private-kb).

### Deployment

This site is published using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

### Search

Search is provided using a local index via https://github.com/praveenn77/docusaurus-lunr-search

**Note**: search only works on production builds, not locally-running developer instances.
