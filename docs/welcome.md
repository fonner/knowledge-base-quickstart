---
slug: /
sidebar_position: 1
---

# Welcome

Welcome to your personal knowledge base site.

# Getting Started

1. Fork the repo at https://gitlab.com/fonner/knowledge-base-quickstart to get your own copy of this code
1. Edit the `docusaurus.config.js` file and edit the `gitlabUser` and `gitlabProject` variables to match your Gitlab user and project.
1. See the README for how to build
1. See the [blog article](https://jedfonner.com/posts/2023-01-22-private-kb) for how to publish

# Tips

Add additional pages to this directory.

Put a line at the top of each file starting with `#` followed by the title of your page. If you do not put a title on your page, then the filename will be used as the title.

If you want to add images or documents (e.g., pdfs) to your knowledge base, create a folder called `assets` and add your images or documents to that folder. Then you can refernece them locally inside your knowledge pages like `assets/<name-of-asset>.ext`.

You can add "front matter" between `---` lines to further customize your pages. See https://docusaurus.io/docs/create-doc#doc-front-matter for more information.

## Assets example

![Knowledge Vault image](assets/knowledge-vault.jpg)

<label><small>OpenAI image for 'Knowledge Vault'</small></label>

## Changes

- Jan 14, 2024: Switches local search to [@easyops-cn/docusaurus-search-local](https://github.com/easyops-cn/docusaurus-search-local)
- Jan 13, 2024: Upgrade to [Docusaurus 3](https://docusaurus.io/docs/migration/v3)
- Feb 14, 2023: Initial release
